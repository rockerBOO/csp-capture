# CSP Capture

Simple CSP capture logger.

## Variables

Set the following environmental variables inside the client. See `.env` for the recommended values. 

```
CSP_CAPTURE_LOG=/log/csp-capture.log
CSP_CAPTURE_PORT=12345
```

## Build

Building into tag csp-capture.

```sh
podman build -t registry.gitlab.com/rockerboo/csp-capture:latest .
```

## Run 


```sh
podman run -v "$(pwd)":/log --env-file .env -p "127.0.0.1:12345:12345" registry.gitlab.com/rockerboo/csp-capture:latest
```

Note:
- Logs to `/log/csp-capture.log`
- HTTP server runs on 12345 by default

