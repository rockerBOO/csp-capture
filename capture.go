package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
)

type Report struct {
	CspReport CspReport `json:"csp-report"`
}

type CspReport struct {
	DocumentURI       string `json:"document-uri"`
	Referrer          string `json:"referrer"`
	BlockedURI        string `json:"blocked-uri"`
	ViolatedDirective string `json:"violated-directive"`
	OriginalPolicy    string `json:"original-policy"`
	Disposition       string `json:"disposition"`
}

func main() {
	logFile, flag := os.LookupEnv("CSP_CAPTURE_LOG")

	if flag != true {
		panic("CSP_CAPTURE_LOG needs to be set")
	}

	port, flag := os.LookupEnv("CSP_CAPTURE_PORT")	

	if flag != true {
		port = "12345"
	}

	f, err := os.OpenFile(logFile, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		panic(fmt.Sprintf("could not open %s", logFile))
	}

	defer f.Close()

	log.SetOutput(f)

	http.HandleFunc("/csp-capture", func(w http.ResponseWriter, r *http.Request) {
		decoder := json.NewDecoder(r.Body)

		var t Report
		err := decoder.Decode(&t)

		if err != nil {
			log.Println("Error processing json")
		} else {
			log.Printf("%s %s violated (%s) %s", t.CspReport.DocumentURI, t.CspReport.BlockedURI, t.CspReport.ViolatedDirective, t.CspReport.OriginalPolicy)
		}
	})

	http.ListenAndServe(fmt.Sprintf(":%s", port), nil)
}
